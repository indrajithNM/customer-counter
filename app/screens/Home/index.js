import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  AsyncStorage,
} from 'react-native';
import {Button, SafeAreaView, TextInput, Text} from '@components';
import {BaseStyle, useTheme, BaseSetting} from '@config';
import {useTranslation} from 'react-i18next';
import {BASE_URL, getKey} from '../../helpers/user';
import crashlytics from '@react-native-firebase/crashlytics';
// import {AsyncStorage} from 'react-native';
let base64 = require('base-64');

export default function Home({navigation}) {
  console.log('home')
  const {t} = useTranslation();
  const {colors} = useTheme();
  const [error, setError] = useState('');
  const [password, setPassword] = useState('');
  const [shop, setShop] = useState('');
  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  const [success, setSuccess] = useState({
    password: true,
    shop: true,
  });
  const [loading, setLoading] = useState(false);
  const onSubmit = () => {
    // crashlytics().crash()
    if (password == '' || shop == '') {
      setSuccess({
        ...success,
        shop: shop != '' ? true : false,
        password: password != '' ? true : false,
      });
    } else {
      setLoading(true);
      let headers = new Headers();
      headers.append(
        'Authorization',
        'Basic ' + base64.encode('apiuser:a22323212'),
      );
      headers.append('Content-Type', 'application/json');
      fetch(`${BASE_URL}/validateAuth`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify({shopId: shop, password}),
      })
        .then(response => response.json())
        .then(async json => {
          setLoading(false);
          if (json.key) {
            await AsyncStorage.setItem('key', json.key);
            console.log(AsyncStorage.getItem('key'));
            navigation.navigate('Dashboard');
          } else {
            setError(json.message);
          }
        });
      // let req = new XMLHttpRequest();
      // req.open('post', `${BaseSetting}/validateAuth`, true);
      // req.setRequestHeader(
      //   'Authorization',
      //   'Basic ' + Base64.encode('apiuser' + ':' + 'a22323212'),
      // );
      // req.setRequestHeader('Content-Type', 'application/json');
      // req.withCredentials = true;
      // req.onreadystatechange = function() {
      //   if (this.readyState == 4 && this.status == 200) {
      //     let res = JSON.parse(this.responseText);
      //     // localStorage.setItem('key', res.key);
      //     // navigation.navigate('Dashboard');
      //     AsyncStorage.setItem('key', res.key);
      //     setLoading(false);
      //     navigation.navigate('Dashboard');
      //     console.log(res);
      //     // history.push('/');
      //     //KTFPZU
      //   }
      // };
      // req.send(JSON.stringify({shopId: shop, password}));
      // setTimeout(() => {
      //   setLoading(false);
      //   navigation.navigate('Dashboard');
      // }, 500);
    }
  };

  /**
   * @description Show icon services on form searching
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   * @returns
   */

  return (
    <SafeAreaView style={BaseStyle.safeAreaView}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'android' ? 'height' : 'padding'}
        keyboardVerticalOffset={offsetKeyboard}
        style={{flex: 1}}>
        <ScrollView
          contentContainerStyle={{
            paddingHorizontal: 20,
            height: '100%',
            flex: 1,
            justifyContent: 'center',
          }}>
          <View style={{marginBottom: 30}}>
            <Text
              style={{
                fontSize: 40,
                textAlign: 'center',
                fontWeight: 'bold',
                color: colors.primary,
                marginBottom: 40,
              }}>
              Login
            </Text>
            <TextInput
              style={{marginTop: 10}}
              onChangeText={text => setShop(text)}
              placeholder={t('Shop ID')}
              success={success.shop}
              value={shop}
            />
            <TextInput
              style={{marginTop: 10}}
              onChangeText={text => setPassword(text)}
              placeholder={t('Password')}
              success={success.password}
              secureTextEntry={true}
              value={password}
            />
          </View>
          <Text
            style={{
              fontSize: 20,
              marginTop: 10,
              marginBottom: 10,
              color: colors.primary,
              textAlign: 'center',
            }}>
            {error}
          </Text>
        </ScrollView>
        <View style={{paddingVertical: 15, paddingHorizontal: 20}}>
          <Button
            loading={loading}
            full
            onPress={() => {
              onSubmit();
            }}>
            {t('Login')}
          </Button>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
