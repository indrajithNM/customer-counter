import React, {useState, useEffect} from 'react';
import {View, KeyboardAvoidingView, Platform, ScrollView} from 'react-native';
import {BaseStyle, useTheme} from '@config';
import {useTranslation} from 'react-i18next';
import {Header, SafeAreaView, Icon, Text, Button, TextInput} from '@components';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import styles from './styles';
import {BASE_URL, getKey} from '../../helpers/user';
import messaging from '@react-native-firebase/messaging';
import iid from '@react-native-firebase/iid';
let base64 = require('base-64');

export default function Dashboard({navigation}) {
  // async function requestUserPermission() {
  //   let enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;
  //   if (enabled) {
  //     console.log('Authorization status:', authStatus);
  //     return;
  //   }
  //   const authStatus = await messaging().requestPermission();
  //   enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  //   if (enabled) {
  //     console.log('Authorization status:', authStatus);
  //   }
  // }

  useEffect(async () => {
    let key = await getKey();
    if (!key) {
      navigation.navigate('Home');
    } else {
      getDetails();
    }

    const id = await iid().get();
    const token = await messaging().getToken();
    console.log('key :'+key)
    let headers = new Headers();
    headers.append(
      'Authorization',
      'Basic ' + base64.encode('apiuser:a22323212'),
    );
    headers.append('Content-Type', 'application/json');
    fetch(`${BASE_URL}/addOrUpdateGuardNotifications`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({
        deviceToken: token.toString(),
        deviceType: 'string',
        guardKey: key.toString(),
        notify: true,
      }),
    })
      .then(response => response.json())
      .then(async json => {
        console.log(json);
        console.log(token);
      });
    // console.log(id);
    // console.log(token);
    // .then(token => {
    //   // return saveTokenToDatabase(token);
    //   console.log('Token ' + token);
    // });
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log(remoteMessage);
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  const {colors} = useTheme();
  const {t} = useTranslation();
  const [currentLimit, setCurrentLimit] = useState(0);
  const [limit, setLimit] = useState(0);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [doorName, setDoorName] = useState('');
  const [shopId, setShopId] = useState('');
  const [success, setSuccess] = useState({
    name: true,
    email: true,
    message: true,
  });
  const [loading, setLoading] = useState(false);
  const [region] = useState({
    latitude: 10.73902,
    longitude: 106.704938,
    latitudeDelta: 0.009,
    longitudeDelta: 0.004,
  });
  // useEffect(async () => {

  // }, []);

  /**
   * @description Called when user sumitted form
   * @author Passion UI <passionui.com>
   * @date 2019-08-03
   */
  const getDetails = async () => {
    let headers = new Headers();
    headers.append(
      'Authorization',
      'Basic ' + base64.encode('apiuser:a22323212'),
    );
    let key = await getKey();
    if (!key) {
      navigation.navigate('Home');
    }
    headers.append('Content-Type', 'application/json');
    fetch(`${BASE_URL}/findDetails/${key}`, {
      method: 'GET',
      headers: headers,
    })
      .then(response => response.json())
      .then(json => {
        if (json.error) {
          navigation.navigate('Home');
        }
        console.log(json.guard);
        setCurrentLimit(json.depotCurrentLimit);
        setLimit(json.depotLimit);
        setDoorName(json.guard.name);
        setShopId(json.guard.shopId);
      });
  };

  const addOne = async () => {
    let headers = new Headers();
    headers.append(
      'Authorization',
      'Basic ' + base64.encode('apiuser:a22323212'),
    );
    let key = await getKey();
    headers.append('Content-Type', 'application/json');
    fetch(`${BASE_URL}/plusOne/${key}`, {
      method: 'GET',
      headers: headers,
    })
      .then(response => response.json())
      .then(json => {
        console.log(json);
        setMessage('Person was let in, recorded successfully');
        setCurrentLimit(json.depotCurrentLimit);
        setLimit(json.depotLimit);
        if (!json.allowPerson) {
          setMessage(json.notAllowReason);
        }
      });
  };
  const minusOne = async () => {
    let headers = new Headers();
    headers.append(
      'Authorization',
      'Basic ' + base64.encode('apiuser:a22323212'),
    );
    let key = await getKey();
    headers.append('Content-Type', 'application/json');
    fetch(`${BASE_URL}/minusOne/${key}`, {
      method: 'GET',
      headers: headers,
    })
      .then(response => response.json())
      .then(json => {
        console.log(json);
        setMessage('Person was let out, recorded successfully');
        setCurrentLimit(json.depotCurrentLimit);
        setLimit(json.depotLimit);
      });
  };

  const offsetKeyboard = Platform.select({
    ios: 0,
    android: 20,
  });
  return (
    <SafeAreaView style={{height: '100%'}}>
      <Header
        title="Home"
        renderRight={() => {
          return (
            <Icon
              name="cog"
              size={20}
              color={colors.primary}
              enableRTL={true}
            />
          );
        }}
        onPressRight={() => {
          navigation.navigate('Profile2');
        }}
      />
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          height: 'auto',
          marginTop: 20,
        }}>
        <Text title2 style={{marginTop: 20, marginBottom: 20}}>
          Door Name : {doorName}
        </Text>
        {/* <Text title2 style={{marginTop: 20, marginBottom: 20}}>
          Shop ID : {shopId}
        </Text> */}
        {/* <Button
          outline
          style={{
            width: '60%',
            borderWidth: 2,
          }}
          onPress={() => {
            navigation.navigate('Profile2');
          }}>
          Profile
        </Button> */}
        <Button
          round
          outline
          style={{
            height: 100,
            width: 100,
            borderRadius: 100,
            borderWidth: 9,
            marginTop: 40,
          }}
          onPress={() => {
            addOne();
          }}>
          <Icon name="plus" style={{fontSize: 40}} size={10} solid />
        </Button>
        <Text
          style={{
            fontSize: 50,
            marginTop: 30,
            marginBottom: 5,
            color: colors.primary,
          }}>
          {currentLimit}/{limit}
        </Text>
        <Button
          round
          outline
          style={{
            height: 100,
            width: 100,
            borderRadius: 100,
            borderWidth: 9,
            marginTop: 30,
          }}
          onPress={() => {
            minusOne();
          }}>
          <Icon name="minus" style={{fontSize: 40}} size={10} solid />
        </Button>
        <Text
          style={{
            fontSize: 15,
            marginTop: 30,
            color: '#000',
          }}>
          {message}
        </Text>
      </View>
    </SafeAreaView>
  );
}
