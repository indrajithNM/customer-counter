/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
// import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
console.log(12345);
import messaging from '@react-native-firebase/messaging';
import App from 'app/index.js';
import {BaseSetting} from '@config';

messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});

AppRegistry.registerComponent(BaseSetting.name, () => App);
